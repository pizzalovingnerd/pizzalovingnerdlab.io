---
title: GitHub passwords stored in plain text
date: 2018-05-01
---
I got home today, and as usual the first thing I need was check my email, then I got this:

![Email From GitHub](https://cdn.pbrd.co/images/Hjf7v0S.png)

GitHub is one of my favorite sites in the world. This website along with all my other programs (such as TermGet) are hosted on GitHub! However, I don't agree with how GitHub handled the issue.

I feel like github handled the situation really well. They didn't try to hide the issue like Yahoo did, and it wasn't even that big. It's not like a group of hackers hacked into GitHub, and stole every password. It was a small minor issue, yet they took big action on it.

The thing that ticked me off was that, they made everyone change their passwords in order to even login to GitHub. This annoyed me because it was a small issue that probably didn't effect anything. While I was gonna change my password anyway, the email its self said that "```Additionally, they were not accessible to the majority of GitHub staff and we have determined that it is very unlikely that any GitHub staff accessed these logs.```" This annoys me because they forced users to change passwords despite the fact that the email clearly said it's unlikely anyone got looked at the logs.

Overall, GitHub takes security very seriously. Then again, they serviced the world's biggest DDoS attack ever recorded. (source: [Wired](https://www.wired.com/story/github-ddos-memcached/))

<script type='text/javascript' src='https://ko-fi.com/widgets/widget_2.js'></script><script type='text/javascript'>kofiwidget2.init('Buy Me a Coffee', '#0d00ff', 'K3K3CS03');kofiwidget2.draw();</script> 
