---
title: How I got stuck in the apple ecosystem
date: 2018-07-17
---
<iframe width="560" height="315" src="https://www.youtube.com/embed/0aSg8oZ1RoY" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>



##### Note: This is the script to a YouTube video.

This video is different than my other videos. This is more of a story video. Enjoy.

Apple used to make good products, however things have gotten down hill. Now instead of making good products, their goal is to trapped you in their ecosystem. I hope this video will help you think differently about Apple.

My dad used to work in the electronic section of a Target store. He sold lots of Apple products such as iPhones and iPads. He loved Apple products. So he bought me an iPod Touch at like age 5-7, I cannot remember the exact age. I played tons of games on it, listen to my favorite songs, it was the best thing ever. Later my mom got rid of her Windows laptop that sounded like it was gonna explode whenever we turned it on, and we got a MacBook Pro. Later my dad got rid of his laptop, and also got a Mac. We were a full Apple family. But at age 9 my Windows XP computer died. I did a lot of gaming on it such as Minecraft, yep I played Minecraft on a Windows XP computer, I only had like 20 frames per second on it, it sucked. Anyways I started playing games on my Mom’s Mac. Soon I discovered Steam, and played lots of games like Portal. But then, I really wanted to play Terreria, but at the time, Terreria only ran on Windows and Linux at the time. This made me really hate Macs. About 2 and a half years later, I got a Windows gaming PC, which I still use to this day (only it runs Linux instead of Windows). Around 2 years before I got my new PC I got an iPad 3. I loved it, and one again bought a lot of games for it. Then in 2015, I got an iPad Air 2 which is what I’m writing this script on. 

About 9 months ago my dad got Apple Music. He loves it, but he got mad cause I couldn’t use it. Apple Music requires iTunes which on Windows is a slow piece of crap that sucks up like half my memory used to play a song on Apple Music. It’s not as bad on Mac, but it sucks. Unfortunately I ran Linux which means I cannot use Apple Music because iTunes isn’t Linux. I could use a VM, but why would I boot up a VM of Windows just to listen to music? I also couldn’t use Apple Music on my phone which is a Windows phone, and my Chromebook. That why I bought Google Play Music which lets you upload your downloaded iTunes library to your play music library for free.

Remember when I said I downloaded lots of games onto my iPads and iPod Touch, well I cannot download those games on Android or Windows Phone. While if you buy something on the Play Store, it only works on Android, there are lots of phones running Android while iPhones are the only one running iOS. When I got a Windows phone, I spend about 12$ to buy games I already had on iOS like Geometry Dash and Minecraft Pocket Edition. Not to mention, if you willing to pay 1$ there is a pretty good Google Play Music client for Windows Phone.

Now here is the big thing that keeps me stuck in the Ecosystem. I use Google Play Music instead of Apple Music, and I could if I really wanted to, just pirate all the games I paid for on iOS, which I believe is morally okay since I already bought it on iOS. However, I have no solution for this. iMessage…

If you don’t use Apple products, iMessage is the app Apple products use for messages. Most people think the whole iMessage thing is stupid, because the only difference is the color of the text messages… Right? Wrong…

If an Android device or Windows phone were to text an iPhone, it would be forced to use Cell Data. But what if it’s a tablet, or if like me, your phone doesn’t have a Sim Card, or what if you want to Text on your computer like you can on Mac. You can’t do any of that without using Apple Products. This means that is if your friend had and iPhone, and you had a phone without a SIM card, you cannot text each other.

One solution is to use a different messaging app like Hangouts, Riot, or Discord. However, I myself refuse to use Hangouts because it sucks, and my dad refuses to use anything isn’t iMessage, which means I have to use my iPad to get a hold of him. This especially sucks because I don’t charge my iPad that often.

So that was how I got trapped in the Apple Ecosystem.

