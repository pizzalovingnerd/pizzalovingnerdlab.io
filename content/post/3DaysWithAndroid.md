---
title: I used Android as my main OS for 3 days. This is what I learned.
date: 2018-08-04
---

Android is the most popular Linux distro in the world. It has taken over the mobile industry, and it's only real compatition is iOS. However, how is Android as a desktop OS? Android on the desktop is unheard of so I decided to try it. For this challenge, I choose [Bliss OS](https://blissroms.com/). I choose Bliss OS over Android x86 because it was based off a newer version of Android, and it was more catered for desktops then Android-x86.

## Why not use Phoenix OS or OpenTHOS?

While I think Phoenix OS is a better looking OS, Phoenix OS and OpenTHOS are both from China. Because of this both Phoenix OS and OpenTHOS don't have any Google apps. Also Phoenix OS has some Chinese bloatware.

## Day 1

On my first day using Bliss OS, I mostly just watched YouTube. I figured out that using a trackpad is pretty much impossible on Bliss OS. I tried using a mouse, and it was a lot better then the trackpad. However the best input device for Android was my drawing tablet.

One thing I liked was using apps instead of websites which made the websites intregate with the system better. Having Google Play Music as an app instead of a website for example, was a lot nicer. However one annoying issue with some of the apps was that some apps (such as Reddit and Quidd) forced portrait mode. While that isn't very bad on a phone or tablet, it's very bad on a computer cause it makes your screen look like this:

![Image](https://geekeasier.com/wp-content/uploads/2014/10/How-to-Change-the-Screen-Orientation-in-Windows-Computer.jpg)
###### [Image Credit Geekeasier](https://geekeasier.com/change-screen-orientation-windows-computer/1707/)

I also noticed that a lot of apps had ads. While that's fine on a mobile device, if a desktop app like Microsoft Word has ads, everyone would stop using it. [Remember what happened with uTorrent](https://torrentfreak.com/utorrent-is-now-ad-supported-and-how-to-disable-121111/)? While the ads the didn't get on my nerves very often, some apps had so many ads they were unusable.

## Day 2

On day 2, I got a lot done. I found a solution to the problem with portrait mode by using an app called [Rotation Control](https://play.google.com/store/apps/details?id=org.crape.rotationcontrol), however some apps like Quidd were broken with this app.

I uninstalled the [Taskbar launcher](https://play.google.com/store/apps/details?id=com.farmerbb.taskbar) because there was an issue where if I click any app on the task bar, no matter what app I clicked on, it would just go to my most recent app. I ended up replacing it with [SquareHome 2](https://play.google.com/store/apps/details?id=com.ss.squarehome2) because I actually really like the tiles in Windows 8 and Windows 8.1.

I also setup [Linux Deploy](https://play.google.com/store/apps/details?id=ru.meefik.linuxdeploy) so that I could get stuff done using GIMP, Kdenlive, and LibreOffice. I was starting to get settled into Android and I was realizing that Android, a mobile operating system, could do about 80% of what my desktop could do. I even wrote my [article about vpns](https://pizzalovingnerd.gitlab.io/post/vpns/) on Android.

# Day 3

Today I mostly watched videos, but I also talked to my friends on Discord, and did a little bit of music and coding. Other then that, I didn't really make an more modifications to my Android setup.

# Conclusion

Running Android as my main OS on my laptop taught me a couple of things about the Android operating system.

Our phones are beginning to do everything our desktops can do. While some may argue that this isn't true, I can get office work done using Google Docs, Microsoft Office for Android, Polaris Office, WPS Office, or Libreoffice inside Linux Deploy. I could even do programming work with apps like DroidEdit, the list goes on. Of course, some things like Graphic Design is hard to do on Android, but we are getting closer and closer.

The other thing I learned is Android is probably the best OS for going offline. On phones, a lot of apps support downloading stuff. I could download shows off Netflix, I can download songs off Google Play Music, (if you're a YouTube Red subscriber) you can even download videos of YouTube. You can't download all that stuff onto your computer, but you can on your phone. Another thing is PC games are all advanced online shooters. Android games are a lot simpler and I'd say you could play 80% of all Android games offline. So if you're going on a trip soon, I recommend putting Android x86 or Bliss OS on a USB drive or an external hard drive; or downloading an Android emulator