---
title: Moving...
date: 2018-10-26
---

I am migrating away from Gitlab. I am now using a self hosted wordpress setup. I also redesigned the website with Wordpress. You can check it out at https://www.pizzalovingnerd.com.

I will also try and upload an article once a week.