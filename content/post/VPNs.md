---
title: Why I don't use a VPN
date: 2018-07-28
---

##  VPNs are a must have for privacy, right? Wrong.

Most VPN companies are data collection companies. So pick your poison, have your ISP which is a huge company collect you data, or have a small vpn company collect you data. I'd rather have an ISP collect my data because smaller companies might get hacked easier.

## What about Private Internet Access or Nord VPN?

Private Internet Access still logs data. They don't use it for anything and it gets deleted after 30 days. What about Nord? Well, Nord also logs data. However, Nord might be your best option because it only logs data for 14 days.

## Pricing

At the time of writing this, Nord has an offer where I can pay 99$ ($2.75 a month) for 3 years of Nord. I don't have 99$. A one year subscription still costs $5.75 a month. I don't have a job. I make all my money off donations, my birthday, christmas, and reselling candy at school. I simply can't afford a VPN.

## What about free VPNs?

All free VPNs collect data. Period. Nothing is free. VPN companies need to make money some how. Free VPNs make money off collecting data. In a capitalist society,    money is everything, so while free VPNs Iook free, you pay with your data.