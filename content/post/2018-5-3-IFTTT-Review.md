---
title: IFTTT is scary
date: 2018-05-03
---

IFTTT is a simple service that lets you connect your devices... It looks cool, but connecting your devices could make it alot easier for your devices to get hacked. Not to mension a huge privacy concern.

First you need to create a IFTTT account, however, this isn't the traditional login where it asks for a email, username, and password. Instead it connects to your Facebook or Google, you know those two companies that are known for spying on you.

Once you sign up, you need to add some applets.

First I got an applet that emails me whenever I take a picture with my phone. So I enable it, take a photo, and bam. I got a notification saying the applet detected an image in my Camera Roll.

![Notifcation](https://cdn.pbrd.co/images/HjxMqQE.jpg)

About a minute later, I got an email with the image.

After that I looked at looked at more applets. "```Automatically get or send an email from Gmail when there's a new item that that matches your eBay search.```". I would test this, but I don't have an Ebay account. What this does, is it tracks everything you search on eBay and emails you when you something is on sale. This is really scary.

"```Add songs from your videos you like to a spotify playlist```". This tracks your liked music videos, and adds it to a spotify playlists. Other applets can log your Android phone calls into spreadsheets, track your location, and tell you when it's gonna rain, change the color of your Philips Hue lightbulb when your Uber arrives, and play music when you get home. This app can track everything about you, and has pretty much full access to your home.

While IFTTT is convenient, the privacy concerns and the security risks out weigh the benefits. It can know pretty much everything about you. IFTTT can also control like your whole house, so I wouldn't use IFTTT.


<script type='text/javascript' src='https://ko-fi.com/widgets/widget_2.js'></script><script type='text/javascript'>kofiwidget2.init('Buy Me a Coffee', '#0d00ff', 'K3K3CS03');kofiwidget2.draw();</script> 
