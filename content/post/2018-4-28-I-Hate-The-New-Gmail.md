---
title: I hate the new Gmail layout...
date: 2018-04-28
---

As you may or may not know, Google has added a new layout to Gmail. I hate this new layout, so much so that I switched to Proton mail. Let me explain why.

![New Gmail](https://cdn.pbrd.co/images/HiMNaIo.png)
![Old Gmail](https://cdn.pbrd.co/images/HiNhlQN.png)

Above are 2 images. The image on the top is the new Gmail layout, and the bottom is the old one.

So, what don't I like about the new Gmail? There's 2 main things.

1. The tabs
2. The side bar

## The Tabs:

I really don't like how the tabs are flat. Below is a screenshot of Chrome
![Chrome Tabs](https://c1.staticflickr.com/4/3234/2830208615_c4d1fc32e8.jpg)

[Image Credit: Drazz from Flickr](https://www.flickr.com/photos/cluefree/2830208615)

On Chrome the tabs the current tab is easier to see because the colors are brighter. Also, the design of the tabs just plain looks better. They even overlap. On the new Gmail layout, finding the current tab takes about 2 seconds longer. The only difference is that the current tab is a line under it. Instead of looking for the contrast in color, you have to look for a ugly looking white line under the tab.

## The Side Bar

The side area on the right is terrible. It only shows icons which just annoys me. I can't even tell what the icons do without moving my mouse over to the side bar to reveal text. On the old layout, it always showed the text. While you can change this using the hamburger menu (the 3 lines) above the bar, it should have to be changed in the first place. On the new YouTube layout, it doesn't show the side bar **unless** you press the hamburger menu. Why can't Gmail be the same way? Another thing is I hate the new compose button. It is super ugly in my opinion.

## Why not just use the old Gmail layout?

When I was telling my dad about how much I hated the new Gmail layout, he asked "Why can't you just use the old [Gmail] Layout".

The reason to this is that sooner or later, Google is gonna remove the old layout. This happened with YouTube. In May of 2017, Google introduced the new YouTube layout, and you could enable it by going to https://www.youtube.com/new. Then in around March of 2018, they quietly got rid of the old layout. While you can get it back with extensions such as Tampermonkey, it's pretty much gone now. My prediction is that this is going to be the same with Gmail. One day they will just quietly get rid of the old Gmail layout.

## My response to this

I am done with Gmail. I've been meaning to leave anyway because of junk mail, but this was the final straw. I ended up signing up for Proton Mail. Proton Mail is a lot better than Gmail so far. For one, it encrypts your emails, and two, it's super customizable, and three, It has one of the nicest looking web clients for any email services which is good for me because I dislike email clients.

<script type='text/javascript' src='https://ko-fi.com/widgets/widget_2.js'></script><script type='text/javascript'>kofiwidget2.init('Buy Me a Coffee', '#0d00ff', 'K3K3CS03');kofiwidget2.draw();</script> 
